package ru.pashalaev.gistesttask1;

import org.mockito.Mockito;
import org.testng.annotations.Test;
import ru.pashalaev.gistesttask1.objects.Thing;
import ru.pashalaev.gistesttask1.objects.Vault;
import ru.pashalaev.gistesttask1.process.FillProcessFullSearch;
import ru.pashalaev.gistesttask1.repos.TaskObjectsRepo;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class FillProcessFullSearchTest {


    public List<Thing> getFilledStore(){
        int[] costArray =   {7,9,8,3,10};
        int[] volumeArray = {6,1,6,2,6};

        return getThings(costArray,volumeArray);
    }


    public List<Thing> getThings(int[] costArray,int[] volumeArray){
        List<Thing> things =  new ArrayList<>();

        for(int i=0;i<volumeArray.length;i++){
            Thing thing = new Thing();
            thing.setCost(costArray[i]);
            thing.setVolume(volumeArray[i]);
            things.add(thing);
        }
        return things;
    }

    public  TaskObjectsRepo getTaskObjectRepoForTest(){
        int volumeVault = 21;
        Vault vault = new Vault();
        vault.setVolume(volumeVault);

        int[] costArray =   {4,5,7,9,1,8,4,6,3,10};
        int[] volumeArray = {4,3,6,1,5,6,7,8,2,6};

        List<Thing> things = getThings(costArray,volumeArray);

        TaskObjectsRepo taskObjectsRepo = Mockito.mock(TaskObjectsRepo.class);
        when(taskObjectsRepo.getThings()).thenReturn(things);
        when(taskObjectsRepo.getVault()).thenReturn(vault);
        return taskObjectsRepo;
    }


    @Test(description = "Тестирование алгоритма полного перебора: значения объема")
    public void testFullSearchAlgVolumeValue(){

        FillProcessFullSearch fillProcessFullSearch = new FillProcessFullSearch();
        Vault fillVault = fillProcessFullSearch.fill(getTaskObjectRepoForTest());

        assertEquals(fillVault.getFreeVolume(),0);
        assertEquals(fillVault.getVolume()-fillVault.getFreeVolume(),21);

    }

    @Test(description = "Тестирование алгоритма полного перебора: значение стоимости")
    public void testFullSearchAlgCostValue(){

        FillProcessFullSearch fillProcessFullSearch = new FillProcessFullSearch();
        Vault fillVault = fillProcessFullSearch.fill(getTaskObjectRepoForTest());

        assertEquals(fillVault.getCost(),37);
    }

    @Test(description = "Тестирование алгоритма полного перебора: значения заполнения")
    public void testFullSearchAlgThingsValue(){

        FillProcessFullSearch fillProcessFullSearch = new FillProcessFullSearch();
        Vault fillVault = fillProcessFullSearch.fill(getTaskObjectRepoForTest());

        assertTrue(fillVault.getStore().containsAll(getFilledStore()));

    }

}
