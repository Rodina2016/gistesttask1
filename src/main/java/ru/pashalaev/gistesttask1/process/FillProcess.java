package ru.pashalaev.gistesttask1.process;


import ru.pashalaev.gistesttask1.objects.Vault;
import ru.pashalaev.gistesttask1.repos.TaskObjectsRepo;


public interface  FillProcess {

   String getName();

   Vault fill(TaskObjectsRepo taskObjectsRepository);

}
