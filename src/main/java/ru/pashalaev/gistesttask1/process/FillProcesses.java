package ru.pashalaev.gistesttask1.process;


public class FillProcesses {

    private FillProcesses(){}

    public static FillProcess getFillProcess(String processName){

        FillProcess fillProcess =null;

        if(processName.equals("FullSearch")){

            fillProcess = new FillProcessFullSearch();
        }

        return fillProcess;

    }
}
