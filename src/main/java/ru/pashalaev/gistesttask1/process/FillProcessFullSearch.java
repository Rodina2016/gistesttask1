package ru.pashalaev.gistesttask1.process;

import ru.pashalaev.gistesttask1.objects.Thing;
import ru.pashalaev.gistesttask1.objects.Vault;
import ru.pashalaev.gistesttask1.repos.TaskObjectsRepo;

import java.util.ArrayList;
import java.util.List;

public class FillProcessFullSearch implements FillProcess {



    public String getName() {
        return "FullSearch";
    }

    @Override
    public Vault fill(TaskObjectsRepo taskObjectsRepo) {


        Vault vault = taskObjectsRepo.getVault();
        List<Thing> things = taskObjectsRepo.getThings();


        FullSearchAlg fullSearchAlg = new FullSearchAlg(vault.getVolume(),things);
        fullSearchAlg.runAlg();

        vault.setStore(fullSearchAlg.getResult());

        return vault;
    }

   //Уберем реализацию рекурсии с ее переменными во внутренний класс
    private class FullSearchAlg{

        private List<Thing> things;
        private List<Thing> bestThings;

        private int vaultVolume;
        private int bestCost;

        FullSearchAlg(int vaultVolume, List<Thing> things){
            this. vaultVolume = vaultVolume;
            this.things = things;


        }

        private void runAlg(){

            runAlg(things);
        }

        private List<Thing> getResult(){

            return bestThings;
        }

        //рекурсия перебора всех возможных комбинаций
        private void runAlg(List<Thing> things)
        {
            if (things.size() > 0)
                checkComb(things);

            for (int i = 0; i < things.size(); i++)
            {
                List<Thing> newThings = new ArrayList<>(things);

                newThings.remove(i);

                runAlg(newThings);
            }


        }
        //Проверим насколько хороша комбинация элементов
        private void checkComb(List<Thing> things)
        {
            if (bestThings == null)
            {
                if (calcVolume(things) <= vaultVolume)
                {
                    bestThings = things;
                    bestCost = calcCost(things);
                }
            }
            else
            {
                if(calcVolume(things) <= vaultVolume && calcCost(things) > bestCost)
                {
                    bestThings = things;
                    bestCost = calcCost(things);
                }
            }
        }

        private int calcVolume(List<Thing> things)
        {
            int sumVol = 0;

            for(int i=0;i<things.size();i++)
                sumVol += things.get(i).getVolume();

            return sumVol;
        }

        private int calcCost(List<Thing> things)
        {
            int sumCost = 0;

            for(int i=0;i<things.size();i++)
                sumCost += things.get(i).getCost();

            return sumCost;
        }
    }





}
