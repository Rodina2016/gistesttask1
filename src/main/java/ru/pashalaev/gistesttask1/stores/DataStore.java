package ru.pashalaev.gistesttask1.stores;

import ru.pashalaev.gistesttask1.objects.Thing;
import ru.pashalaev.gistesttask1.objects.Vault;

import java.util.List;

public interface DataStore {
    List<Thing> getThings();
    Vault getVault();

}
