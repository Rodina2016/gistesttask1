package ru.pashalaev.gistesttask1.stores;



import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.pashalaev.gistesttask1.configs.Config;
import ru.pashalaev.gistesttask1.objects.Settings;
import ru.pashalaev.gistesttask1.objects.Thing;
import ru.pashalaev.gistesttask1.objects.Vault;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JsonStore implements Config, DataStore {


    Settings settings;

    Vault vault;

    List<Thing> things = new ArrayList<>();

    public JsonStore(String fileName) throws IOException {

       loadFromStore (fileName);

    }

    private void loadFromStore(String fileName)throws IOException {

        byte[] jsonData = Files.readAllBytes(Paths.get(fileName));
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(jsonData);

        settings = objectMapper.treeToValue(rootNode.path("settings"), Settings.class);
        vault = objectMapper.treeToValue(rootNode.path("vault"), Vault.class);

        JsonNode thingsNode = rootNode.path("things");
        Iterator<JsonNode> iterator =thingsNode.elements();

        while (iterator.hasNext()){
            things.add(objectMapper.treeToValue(iterator.next(),Thing.class));
        }

    }


    @Override
    public Settings getSettings() {
        return settings;
    }

    @Override
    public List<Thing> getThings() {
        return things;
    }

    @Override
    public Vault getVault() {
        return vault;
    }
}
