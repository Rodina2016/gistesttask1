package ru.pashalaev.gistesttask1;


import ru.pashalaev.gistesttask1.objects.Vault;
import ru.pashalaev.gistesttask1.process.FillProcesses;
import ru.pashalaev.gistesttask1.repos.ConfigRepo;
import ru.pashalaev.gistesttask1.repos.TaskObjectsRepo;
import ru.pashalaev.gistesttask1.process.FillProcess;



import static java.lang.System.exit;

public class Main {
    public static void main(String[] args ){


        if (args.length==0){
            System.out.println("Параметры запуска отсутствуют!");
            exit(1);
        }

        String configSource=args[0];
        String dataSource=args[0];


        TaskObjectsRepo taskObjectsRepo = TaskObjectsRepo.getInstance(dataSource);

        ConfigRepo configRepo = ConfigRepo.getInstance(configSource);

        FillProcess fillProcess = FillProcesses.getFillProcess(configRepo.getSettings().getFillprocname());


        Vault filledVault = fillProcess.fill(taskObjectsRepo);

        filledVault.printVaultInfo();


    }
}
