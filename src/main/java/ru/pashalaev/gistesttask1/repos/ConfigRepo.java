package ru.pashalaev.gistesttask1.repos;

import ru.pashalaev.gistesttask1.configs.Config;
import ru.pashalaev.gistesttask1.configs.JsonConfig;
import ru.pashalaev.gistesttask1.objects.Settings;

public class ConfigRepo {

    private Config config;

    public static ConfigRepo getInstance(String param){

        ConfigRepo configRepo = new ConfigRepo();

        if(param.substring(param.length()-4,param.length()).equals("json"))
        {
            configRepo.config=new JsonConfig(param);
        }

        return configRepo;
    }

    public Settings getSettings(){
        return config.getSettings();
    }

}
