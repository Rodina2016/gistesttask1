package ru.pashalaev.gistesttask1.repos;

import ru.pashalaev.gistesttask1.stores.DataStore;
import ru.pashalaev.gistesttask1.stores.JsonStore;
import ru.pashalaev.gistesttask1.objects.Thing;
import ru.pashalaev.gistesttask1.objects.Vault;
import ru.pashalaev.gistesttask1.stores.OracleStore;


import java.io.IOException;
import java.util.List;

public class TaskObjectsRepo {

    private DataStore dataStore;

    private TaskObjectsRepo(){}

    public static TaskObjectsRepo getInstance(String param){
        TaskObjectsRepo taskObjectsRepo = new TaskObjectsRepo();
        if(param.substring(param.length()-4,param.length()).equals("json")){
            try {

                taskObjectsRepo.dataStore = new JsonStore(param);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(param.contains("jdbs:oracle")){
            taskObjectsRepo.dataStore = new OracleStore(param);
            //значит будем брать данные об объектах из базы
            //это для примера
        }
        return  taskObjectsRepo;

    }

    public List<Thing> getThings() {
        return dataStore.getThings();
    }

    public Vault getVault() {
        return dataStore.getVault();
    }
}
