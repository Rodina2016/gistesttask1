package ru.pashalaev.gistesttask1.objects;

import java.util.List;

public class Vault extends TaskObject{


   private List<Thing> store;

   public void printThingsCost(){

       int cost=getCost();

       if(cost>0)
       System.out.println("Цена всех предметов соcтавляет: " +cost);

    }

    public int getCost(){
        int cost=0;
        for(int i=0;i<store.size();i++) {
            cost += store.get(i).getCost();
        }
        return cost;
    }

    public int getFreeVolume(){
        int freeVolume = getVolume();

        for(int i=0;i<store.size();i++) {
            freeVolume -= store.get(i).getVolume();
        }
        return freeVolume;
    }

    public void printFreeVolume(){

        System.out.println("Свободный объем: " + getFreeVolume());

    }

    public void printVolume(){

        System.out.println("Объем сейфа: " + getVolume());

    }

    public void printThingsList(){
        System.out.println("Предметов в сейфе: " + store.size());

       for(int i=0;i<store.size();i++) {
           Thing thing = store.get(i);
           System.out.println(thing.getName()+"   объем: "+ thing.getVolume() + " цена: "+ thing.getCost());
       }
    }

    public List<Thing> getStore() {
        return store;
    }

    public void setStore(List<Thing> store) {
        this.store = store;
    }

    public void printVaultInfo(){

        printVolume();
        printThingsCost();
        printFreeVolume();
        printThingsList();

    }
}
