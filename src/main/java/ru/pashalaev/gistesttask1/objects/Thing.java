package ru.pashalaev.gistesttask1.objects;

public class Thing extends TaskObject{

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if ( !(obj instanceof Thing))
        return false;

        Thing thing = (Thing) obj;

        return this.getVolume() == thing.getVolume() && this.getCost() == thing.getCost();
    }

    private int cost;

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
