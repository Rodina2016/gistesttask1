package ru.pashalaev.gistesttask1.configs;

import ru.pashalaev.gistesttask1.objects.Settings;
import ru.pashalaev.gistesttask1.stores.JsonStore;

import java.io.IOException;

public class JsonConfig implements  Config{

    private Config jsonStore;

    public JsonConfig (String filename){
        try {
            jsonStore = new JsonStore(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public Settings getSettings() {
        return jsonStore.getSettings();
    }
}
