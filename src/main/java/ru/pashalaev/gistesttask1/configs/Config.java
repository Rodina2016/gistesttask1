package ru.pashalaev.gistesttask1.configs;

import ru.pashalaev.gistesttask1.objects.Settings;


public interface Config {

Settings getSettings();

}
